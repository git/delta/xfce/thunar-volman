# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Chipong Luo <chipong.luo@yahoo.com>, 2011-2012
# Dongyu Chu <chudongyu@126.com>, 2019
# Hunt Xu <huntxu@live.cn>, 2009
# Jiahua Huang <jhuangjiahua@gmail.com>, 2009
# Xiaobo Zhou <zhouxiaobo.500@gmail.com>, 2017
# 玉堂白鹤 <yjwork@qq.com>, 2018,2020
msgid ""
msgstr ""
"Project-Id-Version: Thunar-volman\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-08-19 12:45+0200\n"
"PO-Revision-Date: 2020-08-19 10:45+0000\n"
"Last-Translator: Xfce Bot <transifex@xfce.org>\n"
"Language-Team: Chinese (China) (http://www.transifex.com/xfce/thunar-volman/language/zh_CN/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: zh_CN\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../thunar-volman/main.c:59
msgid "The sysfs path of the newly added device"
msgstr "新增设备的 sysfs 路径"

#: ../thunar-volman/main.c:60
#: ../thunar-volman-settings/thunar-volman-settings.desktop.in.in.h:2
#: ../thunar-volman-settings/tvm-preferences-dialog.c:103
msgid "Configure management of removable drives and media"
msgstr "可移动驱动器和介质的配置管理"

#: ../thunar-volman/main.c:61
msgid "Print version information and exit"
msgstr "显示版本信息后退出"

#. setup application name
#: ../thunar-volman/main.c:93
msgid "Thunar Volume Manager"
msgstr "Thunar 卷管理器"

#: ../thunar-volman/main.c:121
msgid "All rights reserved."
msgstr "保留所有权利。"

#: ../thunar-volman/main.c:122
#, c-format
msgid "Please report bugs to <%s>."
msgstr "请向 <%s> 报告缺陷。"

#: ../thunar-volman/main.c:176
#, c-format
msgid "There is no device with the sysfs path \"%s\""
msgstr "没有 sysfs 路径为 “%s” 的设备"

#: ../thunar-volman/main.c:188
#, c-format
msgid "Must specify the sysfs path of new devices with --device-added"
msgstr "必须用 --device-added 指定新增设备的 sysfs 路径"

#. ...so we need to prompt what to do
#: ../thunar-volman/tvm-block-device.c:204
msgid "Photos and Music"
msgstr "照片和音乐"

#: ../thunar-volman/tvm-block-device.c:205
msgid "Photos were found on your portable music player"
msgstr "在您的便携式音乐播放器里找到了照片"

#: ../thunar-volman/tvm-block-device.c:206
msgid "Would you like to import the photos or manage the music?"
msgstr "您要导入照片或管理音乐吗？"

#: ../thunar-volman/tvm-block-device.c:208
#: ../thunar-volman/tvm-block-device.c:277
#: ../thunar-volman/tvm-block-device.c:379
#: ../thunar-volman/tvm-block-device.c:426
#: ../thunar-volman/tvm-block-device.c:508
#: ../thunar-volman/tvm-block-device.c:838 ../thunar-volman/tvm-run.c:192
#: ../thunar-volman/tvm-run.c:203
msgid "Ig_nore"
msgstr "忽略(_N)"

#: ../thunar-volman/tvm-block-device.c:209
#: ../thunar-volman/tvm-block-device.c:278
msgid "Import _Photos"
msgstr "导入照片(_P)"

#: ../thunar-volman/tvm-block-device.c:210
msgid "Manage _Music"
msgstr "管理音乐(_M)"

#. ask the user to import photos
#: ../thunar-volman/tvm-block-device.c:273
msgid "Photo Import"
msgstr "照片导入"

#: ../thunar-volman/tvm-block-device.c:274
msgid "A photo card has been detected"
msgstr "检测到照片存储卡"

#: ../thunar-volman/tvm-block-device.c:275
msgid ""
"There are photos on the card. Would you like to add these photos to your "
"album?"
msgstr "存储卡中有照片。您想要将这些照片添加到相册吗？"

#. prompt the user to execute the file
#. prompt the user to execute this file
#: ../thunar-volman/tvm-block-device.c:374
#: ../thunar-volman/tvm-block-device.c:421
#, c-format
msgid "Would you like to allow \"%s\" to run?"
msgstr "您要允许 “%s” 运行吗？"

#: ../thunar-volman/tvm-block-device.c:377
#: ../thunar-volman/tvm-block-device.c:424
msgid "Auto-Run Confirmation"
msgstr "自动运行确认"

#: ../thunar-volman/tvm-block-device.c:378
#: ../thunar-volman/tvm-block-device.c:425
msgid "Auto-Run capability detected"
msgstr "检测到自动运行功能"

#: ../thunar-volman/tvm-block-device.c:380
#: ../thunar-volman/tvm-block-device.c:427
msgid "_Allow Auto-Run"
msgstr "允许自动运行(_A)"

#. prompt the user whether to autoopen this file
#: ../thunar-volman/tvm-block-device.c:503
#, c-format
msgid "Would you like to open \"%s\"?"
msgstr "您要打开 “%s” 吗？"

#: ../thunar-volman/tvm-block-device.c:506
msgid "Auto-Open Confirmation"
msgstr "自动打开确认"

#: ../thunar-volman/tvm-block-device.c:507
msgid "Auto-Open capability detected"
msgstr "检测到自动打开功能"

#: ../thunar-volman/tvm-block-device.c:509
msgid "_Open"
msgstr "打开(_O)"

#. generate notification info
#: ../thunar-volman/tvm-block-device.c:617
msgid "CD mounted"
msgstr "CD 已挂载"

#: ../thunar-volman/tvm-block-device.c:618
msgid "The CD was mounted automatically"
msgstr "此 CD 已自动挂载"

#. generate notification info
#: ../thunar-volman/tvm-block-device.c:623
msgid "DVD mounted"
msgstr "DVD 已挂载"

#: ../thunar-volman/tvm-block-device.c:624
msgid "The DVD was mounted automatically"
msgstr "此 DVD 已自动挂载"

#. generate notification info
#: ../thunar-volman/tvm-block-device.c:629
msgid "Blu-ray mounted"
msgstr "蓝光已挂载"

#: ../thunar-volman/tvm-block-device.c:630
msgid "The Blu-ray was mounted automatically"
msgstr "此蓝光已自动挂载"

#: ../thunar-volman/tvm-block-device.c:640
msgid "Volume mounted"
msgstr "卷已挂载"

#: ../thunar-volman/tvm-block-device.c:643
#, c-format
msgid "The volume \"%s\" was mounted automatically"
msgstr "卷 “%s” 已自动挂载"

#: ../thunar-volman/tvm-block-device.c:648
#, c-format
msgid "The inserted volume was mounted automatically"
msgstr "插入的卷已自动挂载"

#: ../thunar-volman/tvm-block-device.c:702
#, c-format
msgid "Unable to locate mount point"
msgstr "未能定位挂载点"

#: ../thunar-volman/tvm-block-device.c:748
#, c-format
msgid "Unable to mount the device"
msgstr "未能挂载此设备"

#: ../thunar-volman/tvm-block-device.c:757
#, c-format
msgid "Could not detect the volume corresponding to the device"
msgstr "无法检测到与设备相对应的卷"

#: ../thunar-volman/tvm-block-device.c:833
msgid "Audio/Data CD"
msgstr "音频/数据 CD"

#: ../thunar-volman/tvm-block-device.c:834
msgid "The CD in the drive contains both music and files"
msgstr "驱动器的 CD 中既有音乐又有文件。"

#: ../thunar-volman/tvm-block-device.c:836
msgid "Would you like to listen to music or browse the files?"
msgstr "您想要播放音乐还是浏览文件？"

#: ../thunar-volman/tvm-block-device.c:839
msgid "_Browse Files"
msgstr "浏览文件(_B)"

#: ../thunar-volman/tvm-block-device.c:840
msgid "_Play CD"
msgstr "播放 CD(_P)"

#: ../thunar-volman/tvm-block-device.c:928
#, c-format
msgid "Unknown block device type \"%s\""
msgstr "未知块设备类型 “%s”"

#: ../thunar-volman/tvm-device.c:139
#, c-format
msgid "Device type \"%s\" not supported"
msgstr "设备类型 “%s” 不支持"

#: ../thunar-volman/tvm-input-device.c:75
msgid "Keyboard detected"
msgstr "检测到键盘"

#: ../thunar-volman/tvm-input-device.c:76
msgid "A keyboard was detected"
msgstr "检测到键盘"

#: ../thunar-volman/tvm-input-device.c:87
#: ../thunar-volman/tvm-input-device.c:101
msgid "Tablet detected"
msgstr "检测到手写板"

#: ../thunar-volman/tvm-input-device.c:88
#: ../thunar-volman/tvm-input-device.c:102
msgid "A graphics tablet was detected"
msgstr "检测到手写板"

#: ../thunar-volman/tvm-input-device.c:111
msgid "Mouse detected"
msgstr "检测到鼠标"

#: ../thunar-volman/tvm-input-device.c:112
msgid "A mouse was detected"
msgstr "检测到鼠标"

#: ../thunar-volman/tvm-input-device.c:141
#, c-format
msgid "Unsupported input device type \"%s\""
msgstr "不支持的输入设备类型 “%s”"

#: ../thunar-volman/tvm-run.c:171
#, c-format
msgid "Autoburning of blank CDs and DVDs is disabled"
msgstr "已禁用自动刻录空白 CD 和 DVD"

#: ../thunar-volman/tvm-run.c:189
msgid "Blank DVD inserted"
msgstr "已插入空白 DVD"

#: ../thunar-volman/tvm-run.c:190
msgid "You have inserted a blank DVD."
msgstr "您插入了一张空白 DVD。"

#: ../thunar-volman/tvm-run.c:191 ../thunar-volman/tvm-run.c:202
msgid "What would you like to do?"
msgstr "您想要做什么？"

#: ../thunar-volman/tvm-run.c:193
msgid "Burn _DVD"
msgstr "刻录 DVD(_D)"

#: ../thunar-volman/tvm-run.c:200
msgid "Blank CD inserted"
msgstr "已插入空白 CD"

#: ../thunar-volman/tvm-run.c:201
msgid "You have inserted a blank CD."
msgstr "您插入了一张空白 CD。"

#: ../thunar-volman/tvm-run.c:204
msgid "Burn _Data CD"
msgstr "刻录数据 CD(_D)"

#: ../thunar-volman/tvm-run.c:205
msgid "Burn _Audio CD"
msgstr "刻录音频 CD(_A)"

#: ../thunar-volman/tvm-run.c:229
#, c-format
msgid "The burn command may not be empty"
msgstr "刻录指令不能为空"

#: ../thunar-volman/tvm-run.c:268
#, c-format
msgid "The CD player command is undefined"
msgstr "未指定 CD 播放器命令"

#: ../thunar-volman/tvm-usb-device.c:62
msgid "Camera detected"
msgstr "检测到相机"

#: ../thunar-volman/tvm-usb-device.c:63
msgid "A photo camera was detected"
msgstr "检测到照相机"

#: ../thunar-volman/tvm-usb-device.c:71
msgid "Printer detected"
msgstr "检测到打印机"

#: ../thunar-volman/tvm-usb-device.c:72
msgid "A USB printer was detected"
msgstr "检测到 USB 打印机"

#: ../thunar-volman/tvm-usb-device.c:100
#, c-format
msgid "Unsupported USB device type \"%s\""
msgstr "不支持的 USB 设备类型 “%s”"

#: ../thunar-volman-settings/main.c:44
msgid "Settings manager socket"
msgstr "设置管理器套接字"

#: ../thunar-volman-settings/main.c:44
msgid "SOCKET ID"
msgstr "套接字 ID"

#. setup application name
#: ../thunar-volman-settings/main.c:60
msgid "Thunar Volume Manager Settings"
msgstr "Thunar 卷管理器设置"

#: ../thunar-volman-settings/thunar-volman-settings.desktop.in.in.h:1
#: ../thunar-volman-settings/tvm-preferences-dialog.c:101
msgid "Removable Drives and Media"
msgstr "可移动驱动器和介质"

#: ../thunar-volman-settings/tvm-command-entry.c:237
msgid "Select an Application"
msgstr "选择应用程序"

#: ../thunar-volman-settings/tvm-command-entry.c:240
msgid "Cancel"
msgstr "取消"

#: ../thunar-volman-settings/tvm-command-entry.c:241
msgid "Select Application"
msgstr "选择应用程序"

#: ../thunar-volman-settings/tvm-command-entry.c:248
msgid "All Files"
msgstr "所有文件"

#: ../thunar-volman-settings/tvm-command-entry.c:253
msgid "Executable Files"
msgstr "可执行文件"

#: ../thunar-volman-settings/tvm-command-entry.c:268
msgid "Perl Scripts"
msgstr "Perl 脚本"

#: ../thunar-volman-settings/tvm-command-entry.c:274
msgid "Python Scripts"
msgstr "Python 脚本"

#: ../thunar-volman-settings/tvm-command-entry.c:280
msgid "Ruby Scripts"
msgstr "Ruby 脚本"

#: ../thunar-volman-settings/tvm-command-entry.c:286
msgid "Shell Scripts"
msgstr "Shell 脚本"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:114
msgid "Help"
msgstr "帮助"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:124
msgid "Close"
msgstr "关闭"

#. Storage
#: ../thunar-volman-settings/tvm-preferences-dialog.c:140
msgid "Storage"
msgstr "存储器"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:151
msgid "Removable Storage"
msgstr "可移动存储器"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:170
msgid "_Mount removable drives when hot-plugged"
msgstr "热插拔时挂载可移动驱动器(_M)"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:177
msgid "Mount removable media when _inserted"
msgstr "插入后挂载可移动介质(_I)"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:184
msgid "B_rowse removable media when inserted"
msgstr "插入后浏览可移动介质(_R)"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:191
msgid "_Auto-run programs on new drives and media"
msgstr "自动运行新增驱动器和介质上的程序(_A)"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:197
msgid "Auto-open files on new drives and media"
msgstr "自动打开新增驱动器和介质上的文件"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:208
msgid "Blank CDs and DVDs"
msgstr "空白 CD 与 DVD"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:227
msgid "_Burn a CD or DVD when a blank disc is inserted"
msgstr "插入空白光盘后刻录 CD 或 DVD(_B)"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:237
msgid "Command for _Data CDs:"
msgstr "数据 CD 的命令(_D)："

#: ../thunar-volman-settings/tvm-preferences-dialog.c:246
msgid "Command for A_udio CDs:"
msgstr "音频 CD 的命令(_U)："

#. Multimedia
#: ../thunar-volman-settings/tvm-preferences-dialog.c:261
msgid "Multimedia"
msgstr "多媒体"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:272
msgid "Audio CDs"
msgstr "音频 CD"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:291
msgid "Play _audio CDs when inserted"
msgstr "插入后播放音频 CD(_A)"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:297
#: ../thunar-volman-settings/tvm-preferences-dialog.c:422
#: ../thunar-volman-settings/tvm-preferences-dialog.c:470
#: ../thunar-volman-settings/tvm-preferences-dialog.c:518
msgid "_Command:"
msgstr "命令(_C)："

#: ../thunar-volman-settings/tvm-preferences-dialog.c:310
msgid "Video CDs/DVDs/Blu-rays"
msgstr "视频 CD/DVD/蓝光"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:329
msgid "Play _video CDs, DVDs, and Blu-rays when inserted"
msgstr "插入后播放视频 CD ，DVD 和蓝光(_V)"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:336
#: ../thunar-volman-settings/tvm-preferences-dialog.c:374
#: ../thunar-volman-settings/tvm-preferences-dialog.c:557
msgid "C_ommand:"
msgstr "命令(_O)："

#: ../thunar-volman-settings/tvm-preferences-dialog.c:349
msgid "Portable Music Players"
msgstr "便携式音乐播放器"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:368
msgid "Play _music files when connected"
msgstr "连接后播放音乐文件(_M)"

#. Cameras
#: ../thunar-volman-settings/tvm-preferences-dialog.c:385
msgid "Cameras"
msgstr "相机"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:396
msgid "Digital Cameras"
msgstr "数码相机"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:415
msgid "Import digital photographs when connected"
msgstr "连接后导入数码照片"

#. Printers
#: ../thunar-volman-settings/tvm-preferences-dialog.c:433
#: ../thunar-volman-settings/tvm-preferences-dialog.c:444
msgid "Printers"
msgstr "打印机"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:463
msgid "Automatically run a program when a _printer is connected"
msgstr "打印机连接后自动运行程序(_P)"

#. Input Devices
#: ../thunar-volman-settings/tvm-preferences-dialog.c:481
msgid "Input Devices"
msgstr "输入设备"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:492
msgid "Keyboards"
msgstr "键盘"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:511
msgid "Automatically run a program when a USB _keyboard is connected"
msgstr "USB 键盘连接后自动运行程序(_K)"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:531
msgid "Mice"
msgstr "鼠标"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:550
msgid "Automatically run a program when a USB _mouse is connected"
msgstr "USB 鼠标连接后自动运行程序(_M)"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:570
msgid "Tablet"
msgstr "手写板"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:589
msgid "Automatically run a program when a _tablet is connected"
msgstr "手写板连接后自动运行程序(_T)"

#: ../thunar-volman-settings/tvm-preferences-dialog.c:596
msgid "Com_mand:"
msgstr "命令(_M)："
